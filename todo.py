#!/usr/bin/env python3
import argparse
from pathlib import Path
import os
from datetime import datetime

# Global variables relating to things that should be referenced globally
TODO_PATH = os.path.join(str(Path.home()), ".todo")
TODO_FILE = "todo.txt"
DONE_FILE = "done.txt"
VERBOSE = False

def add(task, priority='', due=''):

    # Check priority
    if(priority != ''):
        if(priority.isalpha() and len(priority) == 1):
            priority = "(" + priority.toupper() + ")"
        else:
            raise ValueError("Priority must be a character from A-Z or a-z")

    # Get todays date
    date = datetime.today().strftime("%Y-%m-%d")

    # Check due date
    if(due != ''):
        try:
            datetime.strptime(due, '%Y-%m-%d')
        except ValueError:
            raise ValueError("Incorrect due date format, should be YYYY-MM-DD")

        # Properly tag due date as metadata
        due = "due:{}".format(due)

    task = " ".join(task)
    todo_task = "{} {} {} {}".format(priority, date, task, due).strip() + '\n'

    if(VERBOSE):
        print("Adding task: {}".format(todo_task).strip())

    # Open the TODO_FILE
    with open(os.path.join(TODO_PATH, TODO_FILE), 'a') as todo_file:
        todo_file.write(todo_task)

def list_todo():

    with open(os.path.join(TODO_PATH, TODO_FILE), 'r') as todo_file:
        lines = todo_file.readlines()
        for i in range(len(lines)):
            print("{} {}".format(i, lines[i].strip()))

def done(task_number):

    # Get todays date
    date = datetime.today().strftime("%Y-%m-%d")

    tasks = []

    with open(os.path.join(TODO_PATH, TODO_FILE), 'r') as todo_file, open(os.path.join(TODO_PATH, DONE_FILE), 'a') as done_file:
        tasks = todo_file.readlines()

        if(len(tasks) < task_number):
            raise ValueError("The task number given is not in the file")

        done_task = "x {} {}".format(date, tasks[task_number])

        done_file.write(done_task)
        del tasks[task_number]

    with open(os.path.join(TODO_PATH, TODO_FILE), 'w') as todo_file:
        todo_file.write("".join(tasks))

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", help='Use a configuration file other than the default ~/.todo/config', metavar="config_file", default = '')
    parser.add_argument("-f", help='Forces actions without confirmation or interactive input', action='store_true')
    parser.add_argument("-v", help='Prints version', action='store_true')
    parser.add_argument("-V", help="Verbose option", action='store_true')

    action_parser = parser.add_subparsers(dest='command')
    
    add_cmd = action_parser.add_parser('add', aliases=['a'],  help="THING I NEED TO DO +project @context")
    add_cmd.add_argument("-d", nargs="?", type=str, help="The date this needs to be completed by (YYY-MM-DD)", default='')
    add_cmd.add_argument("-p", nargs=1, type=str, help="The priority of a task (A-Z)", default='')
    add_cmd.add_argument('task', nargs="+")

    list_cmd = action_parser.add_parser('list', aliases=['ls'], help="List tasks")

    done_cmd = action_parser.add_parser('done', aliases=['d'], help="Done with a task")
    done_cmd.add_argument("task_number", help="The task number you're done with", type=int)

    args = parser.parse_args()

    global VERBOSE
    VERBOSE = args.V

    if(VERBOSE):
        print("Arguments: " + str(args))

    if(args.d != ''):
        global TODO_PATH
        TODO_PATH = args.d

    if(not os.path.exists(TODO_PATH)):
        if(VERBOSE):
            print("Creating path for todo files: " + TODO_PATH)
        os.makedirs(TODO_PATH)

    if(args.command == 'a' or args.command == 'add'):
        add(args.task, args.p, args.d)

    if(args.command == 'ls' or args.command == 'list'):
        list_todo()

    if(args.command == 'd' or args.command == 'done'):
        done(args.task_number)

if __name__ == "__main__":
    main()
